package com.chirptheboy.disenchanting.blocks;

import com.chirptheboy.disenchanting.Config;
import com.google.common.collect.ImmutableMap;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.IWorldPosCallable;
import net.minecraft.util.IntReferenceHolder;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.SlotItemHandler;
import net.minecraftforge.items.wrapper.InvWrapper;

import javax.annotation.Nonnull;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ThreadLocalRandom;

import static com.chirptheboy.disenchanting.Disenchanting.RARITY_MULTIPLIER;
import static com.chirptheboy.disenchanting.Disenchanting.RARITY_OFFSET;
import static com.chirptheboy.disenchanting.blocks.ModBlocks.DISENCHANTER_CONTAINER;

@SuppressWarnings("ALL")
public class DisenchanterContainer extends Container {

    private TileEntity tileEntity;
    private PlayerEntity playerEntity;
    private IItemHandler playerInventory;
    private Enchantment firstEnchantment;
    private int action; // 0 = show book; 1 = hide book; 2 = take book
    private int experienceCostSlider;
    private boolean requireXP;
    private boolean disenchanter_randomize;
    public final int enchantLevels[] = new int[1];

    public DisenchanterContainer(int windowId, World world, BlockPos pos, PlayerInventory playerInventory, PlayerEntity player) {
        super(DISENCHANTER_CONTAINER, windowId);
        tileEntity = world.getTileEntity(pos);
        this.playerEntity = player;
        this.playerInventory = new InvWrapper(playerInventory);
        action = -1;
        requireXP = Config.DISENCHANTER_REQUIRES_XP.get();
        experienceCostSlider = Config.DISENCHANTER_COST.get();
        disenchanter_randomize = Config.DISENCHANTER_RANDOM_ENCHANTMENT.get();

        this.trackInt(IntReferenceHolder.create(this.enchantLevels, 0));

        // Add slot for enchanted items
        tileEntity.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY).ifPresent(h -> {

            addSlot(new SlotItemHandler(h, 0, 26, 25){

                @Override
                public int getSlotStackLimit() {
                    return 1;
                }

                @Override
                public boolean isItemValid(@Nonnull ItemStack itemStack) {
                    return !EnchantmentHelper.getEnchantments(itemStack).isEmpty();
                }

                @Override
                public ItemStack onTake(PlayerEntity playerEntity, ItemStack itemStack) {

                    // If enchanted item  is removed then remove the enchanted book
                    if (h.getStackInSlot(0).isEmpty()){
                        action = 1;
                    }

                    updateResult();
                    return itemStack;
                }

                @Override
                public void putStack(@Nonnull ItemStack itemStack) {
                    if (!itemStack.isEmpty()){
                        action = 0;
                    }
                    super.putStack(itemStack);
                    updateResult();
                    this.inventory.markDirty();
                }

                @Override
                public void onSlotChanged() {
                    super.onSlotChanged();
                    updateResult();
                }
            });
        });

        // Add slot for books
        tileEntity.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY).ifPresent(h -> {

            addSlot(new SlotItemHandler(h, 1, 80, 25){

                @Override
                public boolean isItemValid(@Nonnull ItemStack itemStack) {
                    return itemStack.getItem() == Items.BOOK;
                }

                @Override
                public ItemStack onTake(PlayerEntity playerEntity, ItemStack itemStack) {

                    // If book are removed then remove the enchanted book
                    if (h.getStackInSlot(1).isEmpty()){
                        action = 1;
                    }
                    updateResult();
                    return itemStack;
                }

                @Override
                public void putStack(@Nonnull ItemStack itemStack) {
                    if (!itemStack.isEmpty()){
                        action = 0;
                    }
                    super.putStack(itemStack);
                    updateResult();
                    this.inventory.markDirty();
                }
            });
        });

        // Add slot for output
        tileEntity.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY).ifPresent(h -> {

            addSlot(new SlotItemHandler(h, 2, 134, 25){

                @Override
                public boolean isItemValid(@Nonnull ItemStack stack) {
                    return false;
                }

                @Override
                public boolean canTakeStack(PlayerEntity playerEntity) {
                    return playerEntity.abilities.isCreativeMode || getHasStack();
                }

                @Override
                public ItemStack onTake(PlayerEntity playerEntity, ItemStack itemStack) {
                    if (!itemStack.isEmpty()) {
                        action = 2;
                    }
                    updateResult();
                    return itemStack;
                }
            });
        });

        layoutPlayerInventorySlots(8, 71);
    }

    @Override
    public void onContainerClosed(PlayerEntity playerEntity) {
        this.inventorySlots.get(2).putStack(ItemStack.EMPTY);
        super.onContainerClosed(playerEntity);
    }

    public String getCostString(){
        return (this.enchantLevels[0] < 0 || playerEntity.abilities.isCreativeMode) ? "" : "Cost: ";
    }

    public String getExperienceCost(){
        return (this.enchantLevels[0] < 0 || playerEntity.abilities.isCreativeMode) ? "" : String.valueOf(this.enchantLevels[0]);
    }

    public int getCostColor(){
        return playerEntity.experienceLevel < this.enchantLevels[0] ? 0xbf634f : 0x3f3f3f;
    }

    public void updateResult(){

        if (!this.playerEntity.world.isRemote) {

            ItemStack enchantedItemStack = this.inventorySlots.get(0).getStack();
            ItemStack bookItemStack = this.inventorySlots.get(1).getStack();

            Map<Enchantment, Integer> enchantmentMap = EnchantmentHelper.getEnchantments(enchantedItemStack);

            //TODO Replace the 'action' variable and switch with just if/else-if statements
            ItemStack newOutputItem;
            switch (action) {

                // Show book
                case 0:
                    if (!enchantedItemStack.isEmpty() && !bookItemStack.isEmpty() && !enchantmentMap.isEmpty()) {

                        int firstEnchantmentLevel = 0;
                        int loopCounter = 0;

                        // Create the iterator, which is used in either case (randomized or first)
                        Iterator<Map.Entry<Enchantment, Integer>> enchantmentIterator = enchantmentMap.entrySet().iterator();

                        // A single enchantment will yield the int 0
                        int enchantmentCount = enchantmentMap.size() - 1;

                        // If randomization is enabled, use a random int, otherwise use 0
                        int randomEnchantment = disenchanter_randomize ? ThreadLocalRandom.current().nextInt(0, enchantmentCount + 1) : 0;

                        // Loop through the enchantment count until the random number is hit, then set the enchantment
                        for (loopCounter = 0; loopCounter <= randomEnchantment; loopCounter++){
                            Map.Entry<Enchantment, Integer> entry = enchantmentIterator.next();
                            if(loopCounter == randomEnchantment) {
                                firstEnchantment = entry.getKey();
                                firstEnchantmentLevel = entry.getValue();
                            }
                        }

                        // Calculate experience cost
                        int rarity = firstEnchantment.getRarity().getWeight();
                        int maxLevel = firstEnchantment.getMaxLevel();
                        this.enchantLevels[0] = requireXP ? (int) ((RARITY_OFFSET - (rarity * RARITY_MULTIPLIER) * (firstEnchantmentLevel / maxLevel)) * experienceCostSlider) : -1;

                        // Only create enchanted book if XP isn't required, or the player has enough experience
                        if (!requireXP || this.enchantLevels[0] <= playerEntity.experienceLevel || playerEntity.abilities.isCreativeMode) {
                            ItemStack newEnchantedBook = new ItemStack(Items.ENCHANTED_BOOK);
                            EnchantmentHelper.setEnchantments(ImmutableMap.of(firstEnchantment, firstEnchantmentLevel), newEnchantedBook);

                            // Put the new enchanted book in the output slot
                            this.inventorySlots.get(2).putStack(newEnchantedBook);

                            // Clear out the action
                            action = -1;
                        }
                    }
                    break;

                // Hide book
                case 1:
                    this.inventorySlots.get(2).putStack(ItemStack.EMPTY);
                    this.enchantLevels[0] = requireXP ? 0 : -1;
                    action = -1;
                    break;

                // Take book
                case 2:
                    // Clear out the action and subtract experience
                    if (requireXP) {
                        playerEntity.addExperienceLevel(-this.enchantLevels[0]);
                        this.enchantLevels[0] = 0;
                    } else {
                        this.enchantLevels[0] = -1;
                    }

                    // Reduce the count of the input book slot
                    this.inventorySlots.get(1).decrStackSize(1);

                    // Remove the first enchantment from the enchanted item
                    enchantmentMap.remove(firstEnchantment);

                    // If enchanted item was a book, we need to do things differently
                    if (enchantedItemStack.getItem() == Items.ENCHANTED_BOOK) {
                        if (enchantmentMap.size() > 0) {
                            newOutputItem = new ItemStack(Items.ENCHANTED_BOOK);
                        } else {
                            newOutputItem = new ItemStack(Items.BOOK);
                        }
                    } else {
                        newOutputItem = enchantedItemStack.copy();
                    }

                    // Re-enchant the original item (this will remove the enchantment tag if the map is empty)
                    EnchantmentHelper.setEnchantments(enchantmentMap, newOutputItem);

                    // Put reduced-enchanted item in the first slot
                    this.inventorySlots.get(0).putStack(newOutputItem);
                    action = -1;
                    break;

                default:
                    break;
            }
            detectAndSendChanges();
        }
    }

    @Override
    public boolean canInteractWith(PlayerEntity playerEntity) {
        return isWithinUsableDistance(IWorldPosCallable.of(Objects.requireNonNull(tileEntity.getWorld()), tileEntity.getPos()), this.playerEntity, ModBlocks.DISENCHANTER);
    }

    private int addSlotRange(IItemHandler handler, int index, int x, int y, int amount, int dx) {
        for (int i = 0; i < amount; i++) {
            addSlot(new SlotItemHandler(handler, index, x, y));
            x += dx;
            index++;
        }
        return index;
    }

    private int addSlotBox(IItemHandler handler, int index, int x, int y, int horAmount, int dx, int verAmount, int dy){
        for (int j = 0; j < verAmount; j++){
            index = addSlotRange(handler, index, x, y, horAmount, dx);
            y += dy;
        }
        return index;
    }

    private void layoutPlayerInventorySlots(int leftCol, int topRow){
        // Player inventory
        addSlotBox(playerInventory, 9, leftCol, topRow, 9, 18, 3, 18);

        // Hotbar inventory
        topRow += 58;
        addSlotRange(playerInventory, 0, leftCol, topRow, 9, 18);
    }

    @Override
    public ItemStack transferStackInSlot(PlayerEntity playerIn, int index) {
        ItemStack itemstack = ItemStack.EMPTY;
        Slot slot = this.inventorySlots.get(index);
        if (slot != null && slot.getHasStack()) {
            ItemStack stack = slot.getStack();
            itemstack = stack.copy();

            // Items from any disenchanter slots (index 0 - 2) should move to either player inventory or hotbar
            if (index < 3) {
                if (index == 2){
                    action = 2;
                }
                if (!this.mergeItemStack(stack, 3, 39, true)) {
                    return ItemStack.EMPTY;
                }
                slot.onSlotChange(stack, itemstack);
            } else {
                // Books should go right to book slot (index 1)
                if (stack.getItem() == Items.BOOK) {
                    if (!this.mergeItemStack(stack, 1, 2, false)) {
                        return ItemStack.EMPTY;
                    }
                } else if (!(EnchantmentHelper.getEnchantments(stack).isEmpty()) && stack.getItem() != Items.BOOK && !(stack.getTranslationKey().matches("item.quark.ancient_tome"))) {
                    // Enchanted items should go right to tool slot (index 0)
                    if (!this.mergeItemStack(stack, 0, 1, false)) {
                        return ItemStack.EMPTY;
                    }
                } else if (index < 30) {
                    if (!this.mergeItemStack(stack, 30, 39, false)) {
                        return ItemStack.EMPTY;
                    }
                } else if (index < 39 && !this.mergeItemStack(stack, 3, 29, false)) {
                    return ItemStack.EMPTY;
                }
            }

            if (stack.isEmpty()) {
                slot.putStack(ItemStack.EMPTY);
            } else {
                slot.onSlotChanged();
            }

            if (stack.getCount() == itemstack.getCount()) {
                return ItemStack.EMPTY;
            }
            slot.onTake(playerIn, stack);
        }
        updateResult();
        return itemstack;
    }
}
