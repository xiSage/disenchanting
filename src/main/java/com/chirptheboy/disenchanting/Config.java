package com.chirptheboy.disenchanting;

import com.electronwill.nightconfig.core.file.CommentedFileConfig;
import com.electronwill.nightconfig.core.io.WritingMode;
import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.config.ModConfig;

import java.nio.file.Path;

@Mod.EventBusSubscriber
public class Config {

    public static final String CATEGORY_GENERAL = "general";

    private static final ForgeConfigSpec.Builder COMMON_BUILDER = new ForgeConfigSpec.Builder();

    public static ForgeConfigSpec COMMON_CONFIG;
    public static ForgeConfigSpec.BooleanValue DISENCHANTER_REQUIRES_XP;
    public static ForgeConfigSpec.BooleanValue DISENCHANTER_RANDOM_ENCHANTMENT;
    public static ForgeConfigSpec.IntValue DISENCHANTER_COST;

    static {

        COMMON_BUILDER.comment("General settings").push(CATEGORY_GENERAL);

        setupFirstBlockConfig();

        COMMON_CONFIG = COMMON_BUILDER.build();
    }

    private static void setupFirstBlockConfig() {

        DISENCHANTER_REQUIRES_XP = COMMON_BUILDER.comment("Does disenchanting cost xp?")
                .define("requires_xp", true);
        DISENCHANTER_COST = COMMON_BUILDER.comment("Relative scale of how much XP the disenchanter costs to use (lower requires less XP)")
                .defineInRange("xp_cost", 2, 1, 10);
        DISENCHANTER_RANDOM_ENCHANTMENT = COMMON_BUILDER.comment("Should the disenchanter select a random enchantment to disenchant?")
                .define("random_enchantment", false);

        COMMON_BUILDER.pop();
    }

    public static void loadConfig(ForgeConfigSpec spec, Path path) {

        final CommentedFileConfig configData = CommentedFileConfig.builder(path)
                .sync()
                .autosave()
                .writingMode(WritingMode.REPLACE)
                .build();

        configData.load();
        spec.setConfig(configData);
    }

    @SubscribeEvent
    public static void onLoad(final ModConfig.Loading configEvent) {

    }

    @SubscribeEvent
    public static void onReload(final ModConfig.ModConfigEvent configEvent) {

    }

}