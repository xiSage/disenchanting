package com.chirptheboy.disenchanting.setup;

import com.chirptheboy.disenchanting.blocks.ModBlocks;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;

public class ModSetup {

    public ItemGroup itemGroup = new ItemGroup("disenchanting") {
        @Override
        public ItemStack createIcon() {
            return new ItemStack(ModBlocks.DISENCHANTER);
        }
    };

    public void init(){

    }
}
